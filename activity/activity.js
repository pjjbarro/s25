db.fruits.aggregate([

		{$match: {supplier: "Red farms"},
		{$count: "fruitsOfRedFarms"}

	])

db.fruits.aggregate([

		{$match: {supplier: "Green farming and Canning"},
		{$count: "fruitsOfGreenFarms"}

	])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"supplier", avg_price: {$avg: "price"}}}

	])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"supplier", max_price: {$max: "$price"}}}

	])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id: "supplier", min_price: {$min: "$price"}}}

	])