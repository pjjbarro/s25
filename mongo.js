db.fruits.insertMany([
  {
    name : "Apple",
    supplier: "Red Farms Inc.",
    stocks : 20,
    price: 40,
    onSale : true,
  },

  {
    name : "Banana",
    supplier : "Yellow Farms",
    stocks : 15,
    price: 20,
    onSale : true,
  },

  {
    name : "Kiwi",
    supplier : "Green Farming and Canning",
    stocks : 25,
    price: 50,
    onSale : true,
  },

  {
    name : "Mango",
    supplier : "Yellow Farms",
    stocks : 10,
    price: 60,
    onSale : true,
  },
  {
    name : "Dragon Fruit",
    supplier: "Red Farms Inc.",
    stocks : 10,
    price: 60,
    onSale : true,
  },

]);

//Aggregation In MongoDB

  //This is the act or the process of generating manipulated data and perform operations to create filtered result that helps in analyzing data.
  //This is helps us in creating reports from analyzing the data provided in our documents.

  //Aggregation Pipelines

    //Aggregation is done in 2-3 steps typically. The first pipeline was with the use of $match

    //$match is used to pass the documents or get the documents that will match our condition.

    //syntax: {$match: {field:value}}

    //$group is used to group elements/documents together and create an analysis of these  grouped documents. In our example, we gathered and grouped our items and solved or got the sum of all their stocks.

    //syntax: {$group: {_id:<id>,fieldResult: "valueResult"}}

    //$sum operator will total the values of all the stock field in the returned documents that were found by passing the $match criteria.

    //$avg operator will average the values of all the given field in the returned documents per group

    // $max will show the highest value out of all the value of a given field in the returned documents per group

    // $min will show the lowest value out of all values of a given field in the returned documents per group.

    //$stocks - is the field that we want to get the sum of. By a $ symbol we will be able to refer to field name that is available in the documents that are being aggregated on.

    //_id: this is us associating an id for the aggregated result. By adding null, we have aggregated and group all matched documents in a single aggregated result.


    //$count stage will allow us to count the total number of items returned by the $match stage
      //syntax: {$count: "string"}

    //$project stage will allows us or hide details from our $group stage
      //syntax: {$project: {_id:0}}

    //$out stage will save the aggregated results to a new collection
      //syntax: {$out: "newCollection"}
    db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"fruitsAggregation", total: {$sum: "$stocks"}}}

    {$set: "aggresiveResults"}
    ])

